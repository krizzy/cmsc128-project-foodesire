class CreateFoods < ActiveRecord::Migration[5.0]
  def change
    create_table :foods do |t|
      t.text :food_name
      t.float :price
      t.text :resto_name
      t.text :description
      t.text :address

      t.timestamps
    end
  end
end
