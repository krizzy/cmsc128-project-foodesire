class CreateTaggings < ActiveRecord::Migration[5.0]
  def change
    if !table_exists?("taggings")
      create_table :taggings do |t|
        t.references :food, foreign_key: true
        t.references :tag, foreign_key: true

        t.timestamps
      end
    end
  end
end
