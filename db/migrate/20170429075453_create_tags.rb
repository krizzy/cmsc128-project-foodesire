class CreateTags < ActiveRecord::Migration[5.0]
  def change
  	if !table_exists?("tags")
	    create_table :tags do |t|
	      t.string :name

	      t.timestamps
	    end
	end
  end

  def down
  	DROP TABLE tags CASCADE;
  end
end
