class CreateHearts < ActiveRecord::Migration[5.0]
  def change
	if !table_exists?("tags")
	    create_table :hearts do |t|
	      t.belongs_to :food, index: true
	      t.belongs_to :user, index: true
	      t.timestamps
	    end
	end
  end
end
