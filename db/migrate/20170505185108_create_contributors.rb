class CreateContributors < ActiveRecord::Migration[5.0]
  def change
    if !table_exists?("contributors")
      create_table :contributors do |t|
        t.references :food, foreign_key: true
        t.references :user, foreign_key: true

        t.timestamps
      end
    end
  end
end
