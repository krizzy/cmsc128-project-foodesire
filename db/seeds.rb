# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

tag1 = Tag.create(name: 'burger')
tag2 = Tag.create(name: 'pizza')
tag3 = Tag.create(name: 'meal')
tag4 = Tag.create(name: 'chicken')

#foodname + price + restoName + descrption + tag
food_list = [
['Champ [Value Meal]', 155.00,'Jollibee', 'Champ na champ!', 'Tacloban City', 'burger'],    
['Pork Sisig [Solo]',  99.00, 'Mang Inasal', 'Sobra Yummy', 'Tacloban City', 'meal'],
['Bangus Inihaw [Solo]', 99.00,'Mang Inasal', 'Sobra Yummy', 'Tacloban City', 'meal'],
['Fullhouse [Quickmelt 11 in.]', 205.00, 'Alberto''s Pizza', 'Pizza is life', 'Tacloban City', 'pizza, meal'],
['Meaty Royale [Quickmelt 9 in.]', 185.00, 'Alberto''s Pizza', 'Pizza is life','Tacloban City', 'pizza'],
['Champ [Solo]', 120.00, 'Jollibee', 'Champ na champ!', 'Tacloban City', 'burger'],
['Cheesy Bacon Mushroom Champ [Solo]', 135.00, 'Jollibee', 'Mushroom cheesy bacon sarap', 'Tacloban City', 'burger'],
['Cheesy Bacon Mushroom Champ [Value Meal]', 169.00, 'Jollibee','Mushroom cheesy bacon sarap', 'Tacloban City', 'burger'],
['1-pc Chickenjoy (Original or Spicy) [Solo]',75.00, 'Jollibee', 'sa jollibee bida ang saya', 'Tacloban City', 'chicken'],
['1-pc Chickenjoy (Original or Spicy) [Value Meal]', 80.00, 'Jollibee', 'sa jollibee bida ang saya', 'Tacloban City', 'meal, chicken'],
['1-pc Chickenjoy (Original or Spicy) [w/ Soup or Side]', 99.00, 'Jollibee', 'sa jollibee bida ang saya', 'Tacloban City', 'meal, chicken'],
['2-pc Chicken Joy (Original or Spicy) [Solo]', 129.00, 'Jollibee', 'sa jollibee bida ang saya', 'Tacloban City', 'meal, chicken'],
['2-pc Chicken Joy (Original or Spicy) [Value Meal]', 139.00, 'Jollibee', 'sa jollibee bida ang saya', 'Tacloban City', 'meal, chicken'],
['1-pc Chickenjoy (Original or Spicy) with Jollibee Spaghetti [Solo]', 105.00, 'Jollibee', 'sa jollibee bida ang saya', 'Tacloban City', 'chicken'],
['1-pc Chickenjoy (Original or Spicy) with Jollibee Spaghetti [Value Meal]', 110.00, 'Jollibee', 'sa jollibee bida ang saya', 'Tacloban City', 'chicken'],
['1-pc Chickenjoy (Original or Spicy) with Palabok [Solo]', 130.00, 'Jollibee', 'Palabok with calamansi!', 'Tacloban City', 'chicken'],
['1-pc Chickenjoy (Original or Spicy) with Palabok [Value Meal]', 135.00, 'Jollibee', 'Palabok with calamansi!', 'Tacloban City', 'chicken'],
['Fullhouse [Quickmelt 11 in.]', 205.00, 'Alberto''s Pizza', 'Ham, salami, hungarian,bacon, etc.', 'Tacloban City', 'pizza, meal'],
['Fullhouse [Mozarella 11 in.]', 249.00, 'Alberto''s Pizza', 'Ham, salami, hungarian,bacon, etc.', 'Tacloban City', 'pizza, meal'],
['Meaty Royale [Quickmelt 11 in.]' , 185.00, 'Alberto''s Pizza', 'Ham, sausage, hungarian,bacon, etc.', 'Tacloban City', 'pizza'],
['Ham Delight [Quickmelt 11 in.]', 117.00, 'Alberto''s Pizza', 'Ham, salami, hungarian,bacon, etc.', 'Tacloban City', 'pizza'],
['All Pepperoni [Quickmelt 11 in.]', 130.00, 'Alberto''s Pizza', 'Ham, salami, hungarian,bacon, etc.', 'Tacloban City', 'pizza']
]

food_list.each do |food_name, price, resto_name, description, address, all_tags|
  Food.create( food_name: food_name, price: price, resto_name: resto_name, description: description, address: address, all_tags: all_tags )
end