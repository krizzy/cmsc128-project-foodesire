class User < ActiveRecord::Base
  has_many :hearts, dependent: :destroy
  has_many :foods, through: :hearts

  has_many :foods, through: :contributors

  # creates a new heart row with food_id and user_id
  def heart!(food)
    self.hearts.create!(food_id: food.id)
  end

  # destroys a heart with matching food_id and user_id
  def unheart!(food)
    heart = self.hearts.find_by_food_id(food.id)
    heart.destroy!
  end

  # returns true or false if a food is hearted by user
  def heart?(food)
    if not food.nil?
      self.hearts.find_by_food_id(food.id)
    end
  end

  def self.from_omniauth(auth)
    where(provider: auth.provider, uid: auth.uid).first_or_initialize do |user|
      user.provider = auth.provider
      user.uid = auth.uid
      user.name = auth.info.name
      user.oauth_token = auth.credentials.token
      user.oauth_expires_at = Time.at(auth.credentials.expires_at)
      user.save!
    end
  end
end
