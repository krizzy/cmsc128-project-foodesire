class Food < ApplicationRecord
  has_many :hearts, dependent: :destroy
  has_many :users, through: :hearts

  has_many :contributors, dependent: :destroy
  has_many :users, through: :contributors

  has_many :taggings, dependent: :destroy
  has_many :tags, through: :taggings

  validates :food_name, presence: true, format: { without: /\A\d+\Z/i, message: 'must not contain only numbers' }
  validates :price, presence: true, numericality: true
  validates :resto_name, presence: true, format: { without: /\A\d+\Z/i, message: 'must not contain only numbers' }
  validates :address, presence: true, format: { without: /\A\d+\Z/i, message: 'must not contain only numbers' }
  validates :all_tags, presence: true, format: { with: /\A([a-z]+)(,\s*[a-z]+)*\Z/i, message: 'Invalid format. Please follow the example above.' }
  validates :description, presence: true, format: { without: /\A\d+\Z/i, message: 'must not contain only numbers' }

  def self.contributed_by(name)
    # TODO: create contributors
    User.find_by(name: name).foods
  end

  def contributor=(name)
    # TODO: create contributors
    users << User.where(name: name)
    #self.contributions = names.split(',').map do |name|
    #  Tag.where(name: name).first_or_create!
    #end
  end

  def all_contributors
    users.map(&:name).join(', ')
  end

  def self.tagged_with(name)
  	Tag.find_by(name: name).foods
  end

  def all_tags=(names)
  	self.tags = names.split(',').map do |name|
  	  Tag.where(name: name).first_or_create!
  	end
  end

  def all_tags
    tags.map(&:name).join(', ')
  end

  def self.search(search)
    num_search = -1
    if numeric? search
      num_search = search.to_f
    end
    Food.joins(:tags).where('food_name ILIKE ? OR price = ? OR resto_name ILIKE ? OR description ILIKE ? OR address ILIKE ? OR name ILIKE ?', ("%" +search + "%"), (num_search), ("%" + search + "%"), ("%" + search + "%"), ("%" + search + "%"), ("%" + search + "%")).distinct
  end

  private

  def self.numeric?(string)
    # `!!` converts parsed number to `true`
    !!Kernel.Float(string) 
    rescue TypeError, ArgumentError
    false
  end
end
