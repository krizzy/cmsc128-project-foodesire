class SessionsController < ApplicationController
  
  def create
    user = User.from_omniauth(request.env['omniauth.auth'])
    session[:user_id] = user.id
    if session[:back_to] == '/foods/show_all'
      redirect_to foods_show_all_path
    elsif session[:back_to] == '/foods/add'
      redirect_to foods_add_path
    elsif session[:back_to] == '/foods/edit'
      redirect_to foods_update_path
    elsif session[:back_to] == '/foods/about'
      redirect_to foods_about_path
    elsif session[:back_to] == '/foods/search'
      redirect_to foods_search_path(search: session[:search])
    else
      redirect_to root_path
    end
  end

  def destroy
    session[:user_id] = nil
    if session[:back_to] == '/foods/show_all'
      redirect_to foods_show_all_path
    elsif session[:back_to] == '/foods/about'
      redirect_to foods_about_path
    elsif session[:back_to] == '/foods/search'
      redirect_to foods_search_path(search: session[:search])
    else
      redirect_to root_path
    end
  end

  def user_params
    params.require(:user).permit(:provider, :uid, :name, :oauth_token, :oauth_expires_at)
  end
end
