class HeartsController < ApplicationController
  respond_to :js

  def heart
    if current_user.nil?
      redirect_to '/auth/facebook'
    else
      @user = current_user
      @food = Food.find(params[:food_id])
      @user.heart!(@food)
    end
  end

  def unheart
    @user = current_user
    @heart = @user.hearts.find_by_food_id(params[:food_id])
    @food = Food.find(params[:food_id])
    @heart.destroy!
  end
end
