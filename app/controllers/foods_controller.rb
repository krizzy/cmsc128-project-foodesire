class FoodsController < ApplicationController
  #before_action :authenticate_user!, only: [:new, :vote]
  respond_to :js, :json, :html

  def food_params
    params.require(:food).permit(:food_name, :price, :resto_name, :description, :address, :all_tags, :all_contributors)
  end
  
  def about
    
  end

  def new
    redirect_to foods_home_path
  end
    
  def home
    session[:name_asc] = true
    session[:likes_asc] = true
    session[:price_asc] = true
  end

  def show_all
    if params[:sort_by] == 'name'
      if session[:name_asc] == true
        @foods = Food.all.to_a.sort! {|a,b| a.food_name <=> b.food_name}
        session[:name_asc] = false
      elsif session[:name_asc] == false
        @foods = Food.all.to_a.sort! {|a,b| b.food_name <=> a.food_name}
        session[:name_asc] = true
      end
    elsif params[:sort_by] == 'likes'
      if session[:likes_asc] == true
        @foods = Food.all.to_a.sort! {|a,b| a.hearts.count <=> b.hearts.count}
        session[:likes_asc] = false
      elsif session[:likes_asc] == false
        @foods = Food.all.to_a.sort! {|a,b| b.hearts.count <=> a.hearts.count}        
        session[:likes_asc] = true
      end
    elsif params[:sort_by] == 'price'
      if session[:price_asc] == true
        @foods = Food.all.to_a.sort! {|a,b| a.price <=> b.price}
        session[:price_asc] = false
      elsif session[:price_asc] == false
        @foods = Food.all.to_a.sort! {|a,b| b.price <=> a.price}
        session[:price_asc] = true
      end
    else
      @foods = Food.all
    end
  end
  
  def index
    
  end

  def search
  #render plain: params[:search].inspect
    session[:search] = params[:search] 
    if params[:search].nil? or params[:search].empty?
      @foods = Food.all
    elsif params[:sort_by] == 'name'
      if session[:name_asc] == true
        @foods = Food.search(params[:search]).to_a.sort! {|a,b| a.food_name <=> b.food_name}
        session[:name_asc] = false
      elsif session[:name_asc] == false
        @foods = Food.search(params[:search]).to_a.sort! {|a,b| b.food_name <=> a.food_name}
        session[:name_asc] = true
      end
    elsif params[:sort_by] == 'likes'
      if session[:likes_asc] == true
        @foods = Food.search(params[:search]).to_a.sort! {|a,b| a.hearts.count <=> b.hearts.count}
        session[:likes_asc] = false
      elsif session[:likes_asc] == false
        @foods = Food.search(params[:search]).to_a.sort! {|a,b| b.hearts.count <=> a.hearts.count}
        session[:likes_asc] = true
      end
    elsif params[:sort_by] == 'price'
      if session[:price_asc] == true
        @foods = Food.search(params[:search]).to_a.sort! {|a,b| a.price <=> b.price}
        session[:price_asc] = false
      elsif session[:price_asc] == false
        @foods = Food.search(params[:search]).to_a.sort! {|a,b| b.price <=> a.price}
        session[:price_asc] = true
      end
    else
      @foods = Food.search(params[:search])  
    end
    if @foods.empty?
      flash[:warning] = "Result not found"
    end
  end
    
  def create
    # render plain: params[:food].inspect
    @food = Food.new(food_params)
    @food.save
    @food.users << current_user
    redirect_to foods_home_path
  end
  
  def update
    food = Food.update(session[:food_id], food_params)
    food.save
    if not food.users.include? current_user
      food.users << current_user
    end
    redirect_to foods_home_path
  end

  def edit

  end

  def destroy
        
  end
  
  def show
    redirect_to foods_search_path
  end

=begin
  def heart
    if !current_user.heart? @food
      @food.hearted_by current_user
    elsif current_user.heart? @food
      @food.unhearted_by current_user
    end
  end
=end
end