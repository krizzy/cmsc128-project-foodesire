Feature: Search food

  As a user
  I want to search the food
  So I that I can find the food
  
  

Scenario: I search a food (happy path)

  Given I am on the homepage
  When I enter a keyword "burger"
  Then I should see the list of foods with the keyword "burger"
  When I click home
  Then I should redirect to homepage

Scenario: I search a food (sad path)

  Given I am on the homepage
  When I enter a keyword "car"
  Then I should see "Result not found"
  When I click home
  Then I should redirect to homepage
