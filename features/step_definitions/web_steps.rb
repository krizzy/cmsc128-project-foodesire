require 'capybara'
require 'cucumber/rails'

Given(/^I am on the homepage$/) do
  visit '/foods/home'
end

When(/^I enter a keyword "([^"]*)"$/) do |keyword|
  fill_in(:search, :with => keyword)
  
  #page.find("#Search").click
  #find('Search').click
  #find('submit[name="search"]').click
  #visit foods_search_path
  click_on 'Search'
end

Then(/^I should see the list of foods with the keyword "([^"]*)"$/) do |keyword|
  page.has_text? keyword
end

When(/^I click home$/) do
  click_link 'Home'
end

Then(/^I should redirect to homepage$/) do
  page.should have_content 'Home'
end

Then(/^I should see "([^"]*)"$/) do |text|
  page.should have_content text
end
