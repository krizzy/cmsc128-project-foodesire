Rails.application.routes.draw do

  post 'heart', to: 'hearts#heart'#, via: :food
  match 'unheart', to: 'hearts#unheart', via: :delete

  resources :hearts
  get 'auth/:provider/callback', to: 'sessions#create'
  get 'auth/failure', to: redirect('/')
  get 'signout', to: 'sessions#destroy', as: 'signout'

  resources :sessions, only: [:create, :destroy]
  
  get 'sessions/create'

  get 'sessions/destroy'

  root 'foods#new'
  get 'foods/home' => 'foods#home'
  get 'foods/home/:id' => 'foods#home'
  #get 'foods/new' => 'foods#new'
  get 'foods/show_all' => 'foods#show_all'
  #post 'foods/show_all' => 'foods#show_all'
  #post 'foods/show_all', to: 'foods#sort_all_by_hearts'
  get 'foods/about' => 'foods#about'
  get 'foods/search' => 'foods#search'
  post 'foods/search' => 'foods#search'
  post 'foods/edit' => 'foods#edit'
  post 'foods/update' => 'foods#update'
  get 'foods/add'



  resources :foods
  
  #get '/pages/track-my-order' => 'pages#track-my-order'
  
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
